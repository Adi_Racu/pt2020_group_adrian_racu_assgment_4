package assigment_4;

public class MonitoredData {
	String start_time;
	String end_time;
	String activiti_label;

	public 	MonitoredData(String start_time, String end_time,String activiti_label ) {
		this.start_time=start_time;
		this.end_time=end_time;
		this.activiti_label=activiti_label;
	}
		
	public String get_start_time() {
		return start_time;
	}	
		
	public String get_end_time() {
		return end_time;
	}	

	public String get_activiti_label() {
		return activiti_label;
	}	
}
